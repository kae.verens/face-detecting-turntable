# face-detecting turntable

turntable that uses face-detection to turn as you move past

## About

I had an idea that it would be cool to have a garden gnome that turned to face whoever was walking past, then just ran with that idea. After some thought, it seemed a better solution was to have a rotating turntable which could thn be mounted with whatever creepy thing you think of - Invader Zim lawn gnome, Doctor Who weeping angel, a standing mirror, a product you are marketing, etc.

Note: this is still a rough draft. It *works*, but it is not pretty. Example: https://twitter.com/kae_verens/status/1480638515637587973

## Hardware

you will need a print of all items in the STL directory. five prints of the wheel.stl

you will also need an ESP32-cam, a battery holder for a 16850 battery (3.7v), an L9110 motor controller, and a geared motor

## Software

the `arduino` directory contains a script for face detection. when a face is detected, it locates the centre of the face, and turns the motor on to turn the table in that direction.
