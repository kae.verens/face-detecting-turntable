#include "esp_camera.h"
#include "fd_forward.h"

#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27
#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23
#define PCLK_GPIO_NUM     22

const int enable_led=0;
const int space=10;
const int center=176;
const int B1A = 12;//define pin 8 for B1A
const int B1B = 13;//define pin 9 for B1B
  
// mtmn_config_t mtmn_config = {0};

static inline mtmn_config_t app_mtmn_config()
{
  mtmn_config_t mtmn_config = {0};
  mtmn_config.type = FAST;
  mtmn_config.min_face = 80;
  mtmn_config.pyramid = 0.707;
  mtmn_config.pyramid_times = 4;
  mtmn_config.p_threshold.score = 0.3;
  mtmn_config.p_threshold.nms = 0.8;
  mtmn_config.p_threshold.candidate_number = 100;
  mtmn_config.r_threshold.score = 0.3;
  mtmn_config.r_threshold.nms = 0.8;
  mtmn_config.r_threshold.candidate_number = 50;
  mtmn_config.o_threshold.score = 0.3;
  mtmn_config.o_threshold.nms = 0.8;
  mtmn_config.o_threshold.candidate_number = 1;
  return mtmn_config;
}
mtmn_config_t mtmn_config = app_mtmn_config();

bool initCamera() {
 
  camera_config_t config;
 
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_GRAYSCALE; // PIXFORMAT_JPEG;
  config.frame_size = FRAMESIZE_CIF;
  config.jpeg_quality = 10;
  config.fb_count = 1;
  
  esp_err_t result = esp_camera_init(&config);

  pinMode(B1A,OUTPUT);// setup motor
  pinMode(B1B,OUTPUT);
  pinMode(4, OUTPUT); // LED
 
  return (result == ESP_OK);
}

 
void setup() {
  Serial.begin(115200);

  Serial.printf("Initializing...\n");

  if (!initCamera()) {
 
    Serial.printf("Failed to initialize camera...\n");
    return;
  }
  sensor_t * s = esp_camera_sensor_get();
  s->set_vflip(s, 1); // flip the camera

   
  // mtmn_config = mtmn_init_config();
  Serial.printf("Ready\n");
}

 
void loop() {
  static unsigned long loopcnt = 0ul;
  static unsigned int detections = 0;
  int x,w,face_center_pan,half_width;

  loopcnt++;
  Serial.printf("Loop %lu\n",loopcnt);
  
  camera_fb_t * frame;
  frame = esp_camera_fb_get();
 
  dl_matrix3du_t *image_matrix = dl_matrix3du_alloc(1, frame->width, frame->height, 3);
  fmt2rgb888(frame->buf, frame->len, frame->format, image_matrix->item);
 
  esp_camera_fb_return(frame);
 
  box_array_t *boxes = face_detect(image_matrix, &mtmn_config);
 
  if (boxes != NULL) {
    detections = detections+1;
    Serial.printf("Faces detected %d times\n", detections);
    if (enable_led) digitalWrite(4, HIGH);

    x = ((int)boxes->box[0].box_p[0]);
    w = (int)boxes->box[0].box_p[2] - x + 1;
    half_width = w / 2;
    int face_center_pan = x + half_width; // image frame face centre x co-ordinate
    Serial.printf("center: %d\n", face_center_pan);

    if (face_center_pan>center-1+space) { // move right
      digitalWrite(B1A,LOW);
      digitalWrite(B1B,HIGH); 
    }
    else if (face_center_pan<center-space) { // move left
      digitalWrite(B1A,HIGH);
      digitalWrite(B1B,LOW); 
    }
    else { // turn off motor
      if (enable_led) digitalWrite(4, LOW);
      digitalWrite(B1A,LOW);
      digitalWrite(B1B,LOW); 
    }
    
    dl_lib_free(boxes->score);
    dl_lib_free(boxes->box);
    dl_lib_free(boxes->landmark);
    dl_lib_free(boxes);
  }
  else {
    if (enable_led) digitalWrite(4, LOW);
    digitalWrite(B1A,LOW);
    digitalWrite(B1B,LOW); 
  }
  dl_matrix3du_free(image_matrix);
}
